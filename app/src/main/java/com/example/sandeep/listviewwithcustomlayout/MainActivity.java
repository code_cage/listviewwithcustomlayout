package com.example.sandeep.listviewwithcustomlayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<ItemModel> list;
    ListView listView;
    CustomListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView=findViewById(R.id.list_view);

        list=new ArrayList<>();
        list.add(new ItemModel(R.drawable.apple,"apple"));
        list.add(new ItemModel(R.drawable.banana,"banana"));
        list.add(new ItemModel(R.drawable.grapes,"grapes"));
        list.add(new ItemModel(R.drawable.lichi,"lichi"));
        list.add(new ItemModel(R.drawable.orange,"orange"));
        list.add(new ItemModel(R.drawable.pineapple,"pineapple"));
        list.add(new ItemModel(R.drawable.pomegranate,"pomegranate"));
        list.add(new ItemModel(R.drawable.strawberry,"strawberry"));

        adapter=new CustomListAdapter(this,list);

        listView.setAdapter(adapter);

    }
}
