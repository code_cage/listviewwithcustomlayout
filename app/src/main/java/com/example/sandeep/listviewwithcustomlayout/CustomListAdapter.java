package com.example.sandeep.listviewwithcustomlayout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Sandeep on 24-Dec-17.
 */

public class CustomListAdapter extends BaseAdapter {

    Context context;
    ArrayList<ItemModel> list;

    public CustomListAdapter(Context context, ArrayList<ItemModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view==null){
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(R.layout.list_item_layout,viewGroup,false);
        }

        ImageView imageView=view.findViewById(R.id.iv_image);
        TextView textView=view.findViewById(R.id.text_view);

        ItemModel model=list.get(i);

        imageView.setImageResource(model.getFruitImage());
        textView.setText(model.getFruitName());

        return view;
    }
}
