package com.example.sandeep.listviewwithcustomlayout;

/**
 * Created by Sandeep on 24-Dec-17.
 */

public class ItemModel {

    int fruitImage;
    String fruitName;

    public ItemModel(int fruitImage, String fruitName) {
        this.fruitImage = fruitImage;
        this.fruitName = fruitName;
    }

    public int getFruitImage() {
        return fruitImage;
    }

    public String getFruitName() {
        return fruitName;
    }
}
